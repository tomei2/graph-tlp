CC=gcc-5
CXX=g++-5
NVCC=nvcc
SRCDIR=src
SOURCES = $(SRCDIR)/mm_io/mm_io.c $(SRCDIR)/bfs.cu
CFLAGS = -std=c++11 -arch=compute_61 -Lsrc/libwb/lib -lwb -Isrc -Isrc/mm_io -Isrc/libwb -g -G -DINPUT_IS_MM
CPU_SOURCES = $(SRCDIR)/mm_io/mm_io.c $(SRCDIR)/bfs.c $(SRCDIR)/readGraph.c $(SRCDIR)/workQueue.c 
CPU_CFLAGS = -std=c99 -Lsrc/libwb/lib -lwb -Isrc -Isrc/mm_io -Isrc/libwb -O3 #-g

.PHONY: bfs
bfs-cuda: 
	$(NVCC) -o bfs $(CFLAGS) $(SOURCES)

bfs-c:
	$(CC) -o bfs $(CPU_CFLAGS) $(CPU_SOURCES) 
	#$(CXX) -E $(CPU_SOURCES) $(CPU_CFLAGS) 

clean:
	rm bfs
