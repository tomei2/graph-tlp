This repo contains for code for evaluating thread level parallelism (tlp) in 
graph applications with a similar methodology to [1]. Currently the only app 
implemented is bfs. Large input graphs show massive available tlp.

[1] http://iss.ices.utexas.edu/Publications/Papers/ppopp2009.pdf