#include "assert.h"
#include "stdbool.h"
#include "stdio.h"
#include "stdlib.h"

#include "readGraph.h"
#include "mm_io.h"

void readMatrixMarket(char *mmInputFile,
                      int *numNodes, int *numNeighbors,
                      unsigned int **nodePtrs, unsigned int **nodeNeighbors) {
  int M, N, nz, *I, *J;
  double *val;
  MM_typecode matcode;
  printf("Reading matrix market file \n");
  int retval = mm_read_mtx_crd(mmInputFile, &M, &N, &nz, &I, &J, &val, &matcode);
  if(retval < 0) {
    printf("matrix read failed!\n");
    exit(-1);
  }

  *numNodes = M;
  *numNeighbors = nz;

  // Initialize node pointers
  *nodePtrs = (unsigned int *)malloc((*numNodes + 1) * sizeof(unsigned int));
  *nodeNeighbors = (unsigned int *)malloc(*numNeighbors * sizeof(unsigned int));

  bool jIsInorder = true;
  for(int j=0; j<nz-1; j++)
    if(J[j] > J[j+1]) jIsInorder = false;

  printf("Converting matrix market to expected input \n");
  unsigned int *nodePtr = *nodePtrs;
  unsigned int *nodeNeighbor = *nodeNeighbors;
  unsigned int n = 0;
  *nodePtr = n;
  if(jIsInorder) {
    unsigned int curJ = 0;
    for(n = 0; n < nz; n++) {
      nodeNeighbor[n] = I[n];
      while(curJ != J[n]) 
        nodePtr[++curJ] = n;
    }
  } else {
    for(int i=1; i<M; i++) {
      nodePtr++;
      for(int ind=0; ind<nz; ind++) {
        if(I[ind] == i) {
          *(nodeNeighbor++) = J[ind];
          n++;
        }
      }
      *(++nodePtr) = n;
    }
  }
  assert((*nodeNeighbors)[0] == (*nodeNeighbors)[1] == 0);
}
