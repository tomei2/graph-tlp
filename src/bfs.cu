#include <stdio.h>
#include <wb.h>
extern "C" {
#include <mm_io.h>
}

#define wbCheck(stmt)							\
  do {									\
    cudaError_t err = stmt;						\
    if (err != cudaSuccess) {						\
      wbLog(ERROR, "Failed to run stmt ", #stmt);			\
      wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));	\
      return -1;							\
    }									\
  } while (0)

#define BLOCK_SIZE 512
#define WARP_SIZE 32
#define NUM_WARPS (BLOCK_SIZE / WARP_SIZE)

// Maximum number of elements that can be inserted into a block queue
#define BQ_CAPACITY 2048

// Maximum number of elements that can be inserted into a warp queue
#define WQ_CAPACITY 128

/******************************************************************************
GPU kernels
*******************************************************************************/

__global__ void gpu_global_queuing_kernel(unsigned int *nodePtrs, unsigned int *nodeNeighbors,
					  unsigned int *nodeVisited,
					  unsigned int *currLevelNodes,
					  unsigned int *nextLevelNodes,
					  unsigned int *numCurrLevelNodes,
					  unsigned int *numNextLevelNodes) {

  // INSERT KERNEL CODE HERE
  // Loop over all nodes in the curent level
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx >= *numCurrLevelNodes) return;

  unsigned int node = currLevelNodes[idx];
  // Loop over all neighbors of the node
  for (unsigned int nbrIdx = nodePtrs[node]; nbrIdx < nodePtrs[node + 1];
       ++nbrIdx) {
    unsigned int neighbor = nodeNeighbors[nbrIdx];
    // If the neighbor hasn't been visited yet
    unsigned int old = atomicExch(nodeVisited+neighbor, 1);
    if (!old) {
      // Mark it and add it to the queue
      unsigned int old = atomicAdd(numNextLevelNodes, 1);
      nextLevelNodes[old] = neighbor;
    }
  }
}

__global__ void gpu_block_queuing_kernel(unsigned int *nodePtrs, unsigned int *nodeNeighbors,
					 unsigned int *nodeVisited,
					 unsigned int *currLevelNodes,
					 unsigned int *nextLevelNodes,
					 unsigned int *numCurrLevelNodes,
					 unsigned int *numNextLevelNodes) {
  // INSERT KERNEL CODE HERE
  // Loop over all nodes in the curent level
  __shared__ int blockNextLevelNodes[BQ_CAPACITY];
  __shared__ int blockNumNextLevelNodes;

  if(threadIdx.x == 0) blockNumNextLevelNodes = 0;
  __syncthreads();

  unsigned int tid = threadIdx.x;
  unsigned int numThreads = blockDim.x * gridDim.x;
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx % (numThreads/(*currLevelNodes)) != 0) return;

  unsigned int node = currLevelNodes[idx / (numThreads/(*currLevelNodes))];
  // Loop over all neighbors of the node
  for (unsigned int nbrIdx = nodePtrs[node]; nbrIdx < nodePtrs[node + 1];
       ++nbrIdx) {
    unsigned int neighbor = nodeNeighbors[nbrIdx];
    // If the neighbor hasn't been visited yet
    unsigned int old = atomicCAS(nodeVisited+neighbor, 0, 1);
    if (!old) {
      // Mark it and add it to the queue
      unsigned int old = atomicAdd(&blockNumNextLevelNodes, 1);
      blockNextLevelNodes[old] = neighbor;
    }
  }

  // global sync
  __syncthreads();
  if(tid == 0) {
    nodeVisited[node] = 2 + blockNumNextLevelNodes;
    atomicAdd(numNextLevelNodes, blockNumNextLevelNodes);
    int globNumNextLevelNodes = 0;
    for (int bid = 0; bid < blockIdx.x; bid++) {
      unsigned int blockNode = currLevelNodes[bid*blockDim.x];
      while(nodeVisited[blockNode] < 2) {};
      globNumNextLevelNodes += nodeVisited[blockNode]-2;
    }
    for (int i = 0; i < blockNumNextLevelNodes; i++) {
      nextLevelNodes[globNumNextLevelNodes+i] = blockNextLevelNodes[i];
    }
  }
}

__global__ void gpu_warp_queuing_kernel(unsigned int *nodePtrs, unsigned int *nodeNeighbors,
					unsigned int *nodeVisited,
					unsigned int *currLevelNodes,
					unsigned int *nextLevelNodes,
					unsigned int *numCurrLevelNodes,
					unsigned int *numNextLevelNodes) {

  // INSERT KERNEL CODE HERE
  // Loop over all nodes in the curent level
  __shared__ int warpNextLevelNodes[WARP_SIZE][WQ_CAPACITY];
  __shared__ int warpNumNextLevelNodes[WARP_SIZE];

  __shared__ int blockNextLevelNodes[BQ_CAPACITY];
  __shared__ int blockNumNextLevelNodes;

  unsigned int tid = threadIdx.x;
  unsigned int wid = tid%WARP_SIZE;
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx >= *numCurrLevelNodes) return;

  unsigned int node = currLevelNodes[idx];
  // Loop over all neighbors of the node
  for (unsigned int nbrIdx = nodePtrs[node]; nbrIdx < nodePtrs[node + 1];
       ++nbrIdx) {
    unsigned int neighbor = nodeNeighbors[nbrIdx];
    // If the neighbor hasn't been visited yet
    unsigned int old = atomicCAS(nodeVisited+neighbor, 0, 1);
    if (!old) {
      // Mark it and add it to the queue
      unsigned int old = atomicAdd(warpNumNextLevelNodes+wid, 1);
      warpNextLevelNodes[wid][old] = neighbor;
    }
  }

  // warp sync
  __syncthreads();
  if(tid == 0) {
    for (int i = 0; i < WARP_SIZE; i++) {
      for (int j = 0; j < warpNumNextLevelNodes[i]; j++) {
	blockNextLevelNodes[blockNumNextLevelNodes+j] = warpNextLevelNodes[i][j];
      }
      blockNumNextLevelNodes += warpNumNextLevelNodes[i];
    }
  }

  // global sync
  if(tid == 0) {
    nodeVisited[node] = 2 + blockNumNextLevelNodes;
    atomicAdd(numNextLevelNodes, blockNumNextLevelNodes);
    int globNumNextLevelNodes = 0;
    for (int bid = 0; bid < blockIdx.x; bid++) {
      unsigned int blockNode = currLevelNodes[bid*blockDim.x];
      while(nodeVisited[blockNode] < 2) {};
      globNumNextLevelNodes += nodeVisited[blockNode]-2;
    }
    for (int i = 0; i < blockNumNextLevelNodes; i++) {
      nextLevelNodes[globNumNextLevelNodes+i] = blockNextLevelNodes[i];
    }
  }
}

/******************************************************************************
Functions
*******************************************************************************/

#define swap_buffers()							\
  do {									\
    printf("Next frontier contains %i nodes\n", *numNextLevelNodes);	\
    *numCurrLevelNodes = *numNextLevelNodes;				\
    *numNextLevelNodes = 0;						\
    unsigned int *temp = currLevelNodes;				\
    currLevelNodes = nextLevelNodes;					\
    nextLevelNodes = temp;						\
  } while (0)

#define swap_buffers_gpu()						\
  do {									\
    cudaMemcpy(numCurrLevelNodes, numNextLevelNodes,			\
	       sizeof(unsigned int), cudaMemcpyDeviceToDevice);		\
    unsigned int *temp = currLevelNodes;				\
    currLevelNodes = nextLevelNodes;					\
    nextLevelNodes = temp;						\
  } while (0)


void cpu_queuing(unsigned int *nodePtrs, unsigned int *nodeNeighbors,
		 unsigned int *nodeVisited, unsigned int *currLevelNodes,
		 unsigned int *nextLevelNodes, unsigned int *numCurrLevelNodes,
		 unsigned int *numNextLevelNodes) {

  while(*numCurrLevelNodes > 0) {
    // Loop over all nodes in the curent level
    for (unsigned int idx = 0; idx < *numCurrLevelNodes; ++idx) {
      unsigned int node = currLevelNodes[idx];
      // Loop over all neighbors of the node
      for (unsigned int nbrIdx = nodePtrs[node]; nbrIdx < nodePtrs[node + 1]; ++nbrIdx) {
	unsigned int neighbor = nodeNeighbors[nbrIdx];
	// If the neighbor hasn't been visited yet
	if (!nodeVisited[neighbor]) {
	  // Mark it and add it to the queue
	  nodeVisited[neighbor] = 1;
	  nextLevelNodes[*numNextLevelNodes] = neighbor;
	  ++(*numNextLevelNodes);
	}
      }
    }
    swap_buffers();
  }
}

void gpu_global_queuing(unsigned int *nodePtrs, unsigned int *nodeNeighbors,
			unsigned int *nodeVisited, unsigned int *currLevelNodes,
			unsigned int *nextLevelNodes, unsigned int *numCurrLevelNodes,
			unsigned int *numNextLevelNodes) {

  const unsigned int numBlocks = 45;
  while(true) {
    gpu_global_queuing_kernel<<<numBlocks, BLOCK_SIZE>>>(nodePtrs, nodeNeighbors, nodeVisited,
							 currLevelNodes, nextLevelNodes,
							 numCurrLevelNodes, numNextLevelNodes);
    unsigned int n;
    swap_buffers_gpu();
    cudaMemcpy(&n, numNextLevelNodes, sizeof(unsigned int), cudaMemcpyDeviceToHost);
    cudaMemset(numNextLevelNodes, 0, sizeof(unsigned int));
    printf("Next frontier contains %i nodes\n", n);	
    if(n == 0) break;
  }
}

void gpu_block_queuing(unsigned int *nodePtrs, unsigned int *nodeNeighbors,
		       unsigned int *nodeVisited, unsigned int *currLevelNodes,
		       unsigned int *nextLevelNodes, unsigned int *numCurrLevelNodes,
		       unsigned int *numNextLevelNodes) {

  const unsigned int numBlocks = 45;
  while(true) {
    gpu_block_queuing_kernel<<<numBlocks, BLOCK_SIZE>>>(nodePtrs, nodeNeighbors, nodeVisited,
							currLevelNodes, nextLevelNodes,
							numCurrLevelNodes, numNextLevelNodes);
    unsigned int n;
    swap_buffers_gpu();
    cudaMemcpy(&n, numNextLevelNodes, sizeof(unsigned int), cudaMemcpyDeviceToHost);
    cudaMemset(numNextLevelNodes, 0, sizeof(unsigned int));
    printf("Next frontier contains %i nodes\n", n);	
    if(n == 0) break;
  }
}

void gpu_warp_queuing(unsigned int *nodePtrs, unsigned int *nodeNeighbors,
		      unsigned int *nodeVisited, unsigned int *currLevelNodes,
		      unsigned int *nextLevelNodes, unsigned int *numCurrLevelNodes,
		      unsigned int *numNextLevelNodes) {

  const unsigned int numBlocks = 45;
  while(true) {
    gpu_warp_queuing_kernel<<<numBlocks, BLOCK_SIZE>>>(nodePtrs, nodeNeighbors, nodeVisited,
						       currLevelNodes, nextLevelNodes,
						       numCurrLevelNodes, numNextLevelNodes);
    unsigned int n;
    swap_buffers_gpu();
    cudaMemcpy(&n, numNextLevelNodes, sizeof(unsigned int), cudaMemcpyDeviceToHost);
    cudaMemset(numNextLevelNodes, 0, sizeof(unsigned int));
    printf("Next frontier contains %i nodes\n", n);	
    if(n == 0) break;
  }
}

void setupProblem(const unsigned int numNodes, unsigned int **nodePtrs_h,
		  unsigned int **nodeNeighbors_h, unsigned int **nodeVisited_h,
		  unsigned int **nodeVisited_ref, unsigned int **currLevelNodes_h,
		  unsigned int **nextLevelNodes_h, unsigned int **numCurrLevelNodes_h,
		  unsigned int **numNextLevelNodes_h, const int *nodePtrs_file,
		  const int *nodeNeighbors_file, const unsigned int data_size,
		  const int *currLevelNodes_file, const unsigned int currentLevelNode_size) {

  // Initialize node pointers
  *nodePtrs_h = (unsigned int *)malloc((numNodes + 1) * sizeof(unsigned int));
  *nodeVisited_h = (unsigned int *)malloc(numNodes * sizeof(unsigned int));
  *nodeVisited_ref = (unsigned int *)malloc(numNodes * sizeof(unsigned int));

  (*nodePtrs_h)[0] = 0;
  for (unsigned int node = 0; node < numNodes; node++) {
    (*nodePtrs_h)[node + 1] = nodePtrs_file[node + 1];
  }

  *nodeNeighbors_h = (unsigned int *)malloc(data_size * sizeof(unsigned int));

  for (unsigned int neighborIdx = 0; neighborIdx < data_size; neighborIdx++) {
    (*nodeNeighbors_h)[neighborIdx] = nodeNeighbors_file[neighborIdx];
  }

  memset((*nodeVisited_h), 0, numNodes * sizeof(int));
  memset((*nodeVisited_ref), 0, numNodes * sizeof(int));

  *numCurrLevelNodes_h = (unsigned int *)malloc(sizeof(unsigned int));
  **numCurrLevelNodes_h = currentLevelNode_size;

  *currLevelNodes_h = (unsigned int *)malloc((**numCurrLevelNodes_h) * sizeof(unsigned int));

  for (unsigned int idx = 0; idx < currentLevelNode_size; idx++) {
    unsigned int node = currLevelNodes_file[idx];
    (*currLevelNodes_h)[idx] = node;
    (*nodeVisited_h)[node] = (*nodeVisited_ref)[node] = 1;
  }

  // Prepare next level containers (i.e. output variables)
  *numNextLevelNodes_h = (unsigned int *)malloc(sizeof(unsigned int));
  **numNextLevelNodes_h = 0;
  *nextLevelNodes_h = (unsigned int *)malloc((numNodes) * sizeof(unsigned int));
}

void setupProblem(char *mmInputFile,
		  unsigned int *numNodes, unsigned int *numNeighbors, unsigned int **nodePtrs_h,
		  unsigned int **nodeNeighbors_h, unsigned int **nodeVisited_h,
		  unsigned int **nodeVisited_ref, unsigned int **currLevelNodes_h,
		  unsigned int **nextLevelNodes_h, unsigned int **numCurrLevelNodes_h,
		  unsigned int **numNextLevelNodes_h) {

  int M, N, nz, *I, *J;
  double *val;
  MM_typecode matcode;
  printf("Reading matrix market file \n");
  mm_read_mtx_crd(mmInputFile, &M, &N, &nz, &I, &J, &val, &matcode);

  *numNodes = M;
  *numNeighbors = nz;

  // Initialize node pointers
  *nodePtrs_h = (unsigned int *)malloc((*numNodes + 1) * sizeof(unsigned int));
  *nodeVisited_h = (unsigned int *)malloc(*numNodes * sizeof(unsigned int));
  *nodeVisited_ref = (unsigned int *)malloc(*numNodes * sizeof(unsigned int));
  *nodeNeighbors_h = (unsigned int *)malloc(*numNeighbors * sizeof(unsigned int));

  bool jIsInorder = true;
  for(int j=0; j<nz-1; j++)
    if(J[j] > J[j+1]) jIsInorder = false;

  printf("Converting matrix market to expected input \n");
  unsigned int *nodePtr = *nodePtrs_h;
  unsigned int *nodeNeighbors = *nodeNeighbors_h;
  unsigned int n = 0;
  *nodePtr = n;
  if(jIsInorder) {
    unsigned int curJ = 0;
    for(n = 0; n < nz; n++) {
      nodeNeighbors[n] = I[n];
      while(curJ != J[n]) 
	nodePtr[++curJ] = n;
    }
  } else {
    for(int i=1; i<M; i++) {
      nodePtr++;
      for(int ind=0; ind<nz; ind++) {
	if(I[ind] == i) {
	  *(nodeNeighbors++) = J[ind];
	  n++;
	}
      }
      *(++nodePtr) = n;
    }
  }

  memset((*nodeVisited_h), 0, *numNodes * sizeof(int));
  memset((*nodeVisited_ref), 0, *numNodes * sizeof(int));

  *numCurrLevelNodes_h = (unsigned int *)malloc(sizeof(unsigned int));
  **numCurrLevelNodes_h = 1;

  *currLevelNodes_h = (unsigned int *)malloc((*numNodes) * sizeof(unsigned int));

  (*currLevelNodes_h)[0] = J[0];
  (*nodeVisited_h)[J[0]] = (*nodeVisited_ref)[J[0]] = 1;

  // Prepare next level containers (i.e. output variables)
  *numNextLevelNodes_h = (unsigned int *)malloc(sizeof(unsigned int));
  **numNextLevelNodes_h = 0;
  *nextLevelNodes_h = (unsigned int *)malloc((*numNodes) * sizeof(unsigned int));
}

int main(int argc, char *argv[]) {

  wbArg_t args;

  args = wbArg_read(argc, argv);
  std::cerr << "args read\n";
  // Initialize host variables
  // ----------------------------------------------

  // Variables
  unsigned int numNodes;
  unsigned int numNeighbors;
  unsigned int *nodePtrs_h;
  unsigned int *nodeNeighbors_h;
  unsigned int *nodeVisited_h;
  unsigned int *nodeVisited_ref;
  unsigned int *currLevelNodes_h;
  unsigned int *nextLevelNodes_h;
  unsigned int *numCurrLevelNodes_h;
  unsigned int *numNextLevelNodes_h;
  unsigned int *nodePtrs_d;
  unsigned int *nodeNeighbors_d;
  unsigned int *nodeVisited_d;
  unsigned int *currLevelNodes_d;
  unsigned int *nextLevelNodes_d;
  unsigned int *numCurrLevelNodes_d;
  unsigned int *numNextLevelNodes_d;

  ///////////////////////////////////////////////////////

  enum Mode { CPU = 1, GPU_GLOBAL_QUEUE, GPU_BLOCK_QUEUE, GPU_WARP_QUEUE };
  Mode mode;

  mode = (Mode)wbImport_flag(wbArg_getInputFile(args, 0));

#ifdef INPUT_IS_MM
  wbTime_start(Generic, "Setting up the problem...");
  setupProblem(wbArg_getInputFile(args, 1), //matrix market input file
	       &numNodes, &numNeighbors, &nodePtrs_h, &nodeNeighbors_h, &nodeVisited_h, &nodeVisited_ref,
	       &currLevelNodes_h, &nextLevelNodes_h, &numCurrLevelNodes_h,
	       &numNextLevelNodes_h);
  wbTime_stop(Generic, "Setting up the problem...");
#else
  int inputLength1; // Total number of Nodes + 1
  int inputLength2; // Total number of Neighbor nodes
  int inputLength3; // Total number of nodes in the current level
  int *hostInput1;  // Node Pointers
  int *hostInput2;  // Node Neighbors
  int *hostInput3;  // Current level Nodes

  wbTime_start(Generic, "Importing data and creating memory on host");

  hostInput1 = (int *)wbImport(wbArg_getInputFile(args, 1), &inputLength1, "Integer");

  // Total  number of nodes in the graph
  numNodes = inputLength1 - 1;

  hostInput2 = numNeighbors = (int *)wbImport(wbArg_getInputFile(args, 2), &inputLength2, "Integer");

  hostInput3 = (int *)wbImport(wbArg_getInputFile(args, 3), &inputLength3, "Integer");

  wbTime_stop(Generic, "Importing data and creating memory on host");

  wbTime_start(Generic, "Setting up the problem...");

  // Initialize graph from imput files and setting up pointers
  setupProblem(numNodes, &nodePtrs_h, &nodeNeighbors_h, &nodeVisited_h, &nodeVisited_ref,
	       &currLevelNodes_h, &nextLevelNodes_h, &numCurrLevelNodes_h,
	       &numNextLevelNodes_h, hostInput1, hostInput2, inputLength2, hostInput3,
	       inputLength3);

  wbTime_stop(Generic, "Setting up the problem...");
#endif

  // Allocate device variables
  // ----------------------------------------------

  if (mode != CPU) {

    wbTime_start(GPU, "Allocating GPU memory.");

    wbCheck(cudaMalloc((void **)&nodePtrs_d, (numNodes + 1) * sizeof(unsigned int)));

    wbCheck(cudaMalloc((void **)&nodeVisited_d, numNodes * sizeof(unsigned int)));

    wbCheck(cudaMalloc((void **)&nodeNeighbors_d, numNeighbors * sizeof(unsigned int)));

    wbCheck(cudaMalloc((void **)&numCurrLevelNodes_d, sizeof(unsigned int)));

    wbCheck(
	    cudaMalloc((void **)&currLevelNodes_d, (numNodes) * sizeof(unsigned int)));

    wbCheck(cudaMalloc((void **)&numNextLevelNodes_d, sizeof(unsigned int)));

    wbCheck(cudaMalloc((void **)&nextLevelNodes_d, (numNodes) * sizeof(unsigned int)));

    wbTime_stop(GPU, "Allocating GPU memory.");
  }

  // Copy host variables to device
  // ------------------------------------------

  if (mode != CPU) {
    wbTime_start(GPU, "Copying input memory to the GPU.");

    wbCheck(cudaMemcpy(nodePtrs_d, nodePtrs_h, (numNodes + 1) * sizeof(unsigned int),
		       cudaMemcpyHostToDevice));

    wbCheck(cudaMemcpy(nodeVisited_d, nodeVisited_h, numNodes * sizeof(unsigned int),
		       cudaMemcpyHostToDevice));

    wbCheck(cudaMemcpy(nodeNeighbors_d, nodeNeighbors_h, numNeighbors * sizeof(unsigned int),
		       cudaMemcpyHostToDevice));

    wbCheck(cudaMemcpy(numCurrLevelNodes_d, numCurrLevelNodes_h, sizeof(unsigned int),
		       cudaMemcpyHostToDevice));

    wbCheck(cudaMemcpy(currLevelNodes_d, currLevelNodes_h,
		       (*numCurrLevelNodes_h) * sizeof(unsigned int), cudaMemcpyHostToDevice));

    wbCheck(cudaMemset(nextLevelNodes_d, 0, (numNodes) * sizeof(unsigned int)));

    wbCheck(cudaMemset(numNextLevelNodes_d, 0, sizeof(unsigned int)));

    wbTime_stop(GPU, "Copying input memory to the GPU.");
  }

  // Launch kernel
  // ----------------------------------------------------------

  printf("Launching kernel \n");

  if (mode == CPU) {
    wbTime_start(Compute, "Performing CPU queuing computation");
    cpu_queuing(nodePtrs_h, nodeNeighbors_h, nodeVisited_h, currLevelNodes_h, nextLevelNodes_h,
		numCurrLevelNodes_h, numNextLevelNodes_h);
    wbTime_stop(Compute, "Performing CPU queuing computation");
  } else if (mode == GPU_GLOBAL_QUEUE) {
    wbTime_start(Compute, "Performing GPU global queuing computation");
    gpu_global_queuing(nodePtrs_d, nodeNeighbors_d, nodeVisited_d, currLevelNodes_d,
		       nextLevelNodes_d, numCurrLevelNodes_d, numNextLevelNodes_d);
    cudaDeviceSynchronize();
    wbTime_stop(Compute, "Performing GPU global queuing computation");
  } else if (mode == GPU_BLOCK_QUEUE) {
    wbTime_start(Compute, "Performing GPU block global queuing computation");
    gpu_block_queuing(nodePtrs_d, nodeNeighbors_d, nodeVisited_d, currLevelNodes_d,
		      nextLevelNodes_d, numCurrLevelNodes_d, numNextLevelNodes_d);
    cudaDeviceSynchronize();
    wbTime_stop(Compute, "Performing GPU block global queuing computation");
  } else if (mode == GPU_WARP_QUEUE) {
    wbTime_start(Compute, "Performing GPU warp global queuing computation");
    gpu_warp_queuing(nodePtrs_d, nodeNeighbors_d, nodeVisited_d, currLevelNodes_d,
		     nextLevelNodes_d, numCurrLevelNodes_d, numNextLevelNodes_d);
    cudaDeviceSynchronize();
    wbTime_stop(Compute, "Performing GPU warp global queuing computation");
  } else {
    // printf("Invalid mode!\n");
    // exit(0);
  }

  // Copy device variables from host
  // ----------------------------------------

  if (mode != CPU) {

    wbTime_start(Copy, "Copying output memory to the CPU");

    wbCheck(cudaMemcpy(numNextLevelNodes_h, numNextLevelNodes_d, sizeof(unsigned int),
		       cudaMemcpyDeviceToHost));

    wbCheck(cudaMemcpy(nextLevelNodes_h, nextLevelNodes_d, numNodes * sizeof(unsigned int),
		       cudaMemcpyDeviceToHost));

    wbTime_stop(Copy, "Copying output memory to the CPU");
  }

  // Verify correctness
  // -----------------------------------------------------

  // for(int ind=0; ind < *numNextLevelNodes_h; ind++) {
  //   printf("%i\n", nextLevelNodes_h[ind]);
  // }
  
  // sorting output to compare the frontier
  wbSort(nextLevelNodes_h, numNodes);

  wbTime_start(Generic, "Verifying results...");

  wbSolution(args, (int *)nextLevelNodes_h, *numNextLevelNodes_h);

  wbTime_stop(Generic, "Verifying results...");

  // Free memory
  // ------------------------------------------------------------

  free(nodePtrs_h);
  free(nodeVisited_h);
  free(nodeVisited_ref);
  free(nodeNeighbors_h);
  free(numCurrLevelNodes_h);
  free(currLevelNodes_h);
  free(numNextLevelNodes_h);
  free(nextLevelNodes_h);
  if (mode != CPU) {
    wbTime_start(GPU, "Freeing GPU Memory");
    cudaFree(nodePtrs_d);
    cudaFree(nodeVisited_d);
    cudaFree(nodeNeighbors_d);
    cudaFree(numCurrLevelNodes_d);
    cudaFree(currLevelNodes_d);
    cudaFree(numNextLevelNodes_d);
    cudaFree(nextLevelNodes_d);
    wbTime_stop(GPU, "Freeing GPU Memory");
  }

  return 0;
}
