#include "stdio.h"
#include "stdlib.h"
#include "workQueue.h"

workItem_t *curWorkQueue = NULL;
workItem_t *freeWorkQueue = NULL;
workItem_t *otherWorkQueue = NULL;

void addWorkItem(workItem_t **workQueue, int nodeInd, int incWeight) {
  workItem_t *curWorkItem;
  if(freeWorkQueue) {
    curWorkItem = freeWorkQueue;
    freeWorkQueue = freeWorkQueue->next;
  } else
    curWorkItem = malloc(sizeof(workItem_t));
  curWorkItem->next = *workQueue;
  curWorkItem->nodeInd = nodeInd;
  curWorkItem->incWeight = incWeight;
  *workQueue = curWorkItem;
}

void getWorkItem(workItem_t **workQueue, workItem_t **curWorkItem) {
  if(*workQueue == NULL) *curWorkItem = NULL;
  else {
    *curWorkItem = *workQueue;
    *workQueue = (*curWorkItem)->next;
  }
}

void freeWorkItem(workItem_t *curWorkItem) {
  curWorkItem->next = freeWorkQueue;
  freeWorkQueue = curWorkItem;
}

void swapWorkQueues() {
  workItem_t *temp = curWorkQueue;
  curWorkQueue = otherWorkQueue;
  otherWorkQueue = temp;
}

