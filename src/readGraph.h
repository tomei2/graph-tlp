#ifndef READGRAPH_H
#define READGRAPH_H

void readMatrixMarket(char *mmInputFile,
                      int *numNodes, int *numNeighbors,
                      unsigned int **nodePtrs, unsigned int **nodeNeighbors);

#endif
