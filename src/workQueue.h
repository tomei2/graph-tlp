#ifndef WORKQUEUE_H
#define WORKQUEUE_H

typedef struct workItem {
  int nodeInd;
  int incWeight;
  struct workItem *next;
} workItem_t;

extern workItem_t *curWorkQueue; 
extern workItem_t *freeWorkQueue; 
extern workItem_t *otherWorkQueue; 

void addWorkItem(workItem_t **workQueue, int nodeInd, int incWeight);
void getWorkItem(workItem_t **workQueue, workItem_t **curWorkItem);
void freeWorkItem(workItem_t *curWorkItem);
void swapWorkQueues();

#endif
