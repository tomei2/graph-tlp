#include "assert.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "readGraph.h"
#include "workQueue.h"

int main(int argc, char **argv) {
  int numNodes, numNeighbors;
  unsigned int *nodePtrs, *nodeNeighbors;
  char * mmInputFile = argv[1];
  readMatrixMarket(mmInputFile,
                   &numNodes, &numNeighbors, &nodePtrs, &nodeNeighbors);

  unsigned int *nodeWeights;
  nodeWeights = (unsigned int *)malloc(numNodes * sizeof(unsigned int));
  memset((nodeWeights), 0, numNodes * sizeof(int));

  addWorkItem(&curWorkQueue, 1, 0);

  int curIterCount = 0;
  while(curWorkQueue != NULL || otherWorkQueue != NULL) {
    if(curWorkQueue == NULL) {
      printf("%i\n", curIterCount);
      swapWorkQueues();
      curIterCount = 0;
    }
    curIterCount++;
    //printf("%i\n", curIterCount);
    workItem_t *curWorkItem;
    getWorkItem(&curWorkQueue, &curWorkItem);
    assert(curWorkItem->nodeInd <= numNodes);
    assert(curWorkItem->incWeight+1 > 0);
    if(curWorkItem->incWeight+1 < nodeWeights[curWorkItem->nodeInd] ||
        0 == nodeWeights[curWorkItem->nodeInd]) {
      nodeWeights[curWorkItem->nodeInd] = curWorkItem->incWeight+1;
      for(int ind = nodePtrs[curWorkItem->nodeInd]; ind < nodePtrs[curWorkItem->nodeInd+1]; ind++) {
        assert(ind < numNeighbors);
        addWorkItem(&otherWorkQueue, nodeNeighbors[ind], nodeWeights[curWorkItem->nodeInd]);
      }
    }
    freeWorkItem(curWorkItem);
  }
}
